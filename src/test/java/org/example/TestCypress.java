package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCypress {

    @Test
    public void testGoogle() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://music.youtube.com/");

        WebElement exploreButton = driver.findElement(By.xpath("/html/body/ytmusic-app/ytmusic-app-layout/ytmusic-nav-bar/div[2]/ytmusic-pivot-bar-renderer/ytmusic-pivot-bar-item-renderer[2]/yt-formatted-string"));
        exploreButton.click();

        WebElement seeAllButton = driver.findElement(By.xpath("/html/body/ytmusic-app/ytmusic-app-layout/div[3]/ytmusic-browse-response/ytmusic-section-list-renderer/div[2]/ytmusic-carousel-shelf-renderer[1]/ytmusic-carousel-shelf-basic-header-renderer/h2/div/div/yt-button-renderer/a/paper-button/yt-formatted-string"));
        seeAllButton.click();

        WebElement topArtists =driver.findElement(By.xpath("/html/body/ytmusic-app/ytmusic-app-layout/div[3]/ytmusic-browse-response/ytmusic-section-list-renderer/div[2]/ytmusic-carousel-shelf-renderer[2]/ytmusic-carousel-shelf-basic-header-renderer/h2/div/yt-formatted-string"));
        Assert.assertEquals("Good vibes only", topArtists.getText());
        driver.quit();
    }

}
